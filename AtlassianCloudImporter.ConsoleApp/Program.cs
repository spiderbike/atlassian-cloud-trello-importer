﻿using AtlassianCloudImporter.ImportTools;
using Figgle;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace AtlassianCloudImporter.ConsoleApp
{
    class MainService : IHostedService
    {
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly ImportHelper _importHelper;

        public MainService(IHostApplicationLifetime appLifetime, ImportHelper importHelper)
        {
            _appLifetime = appLifetime;
            _importHelper = importHelper;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            await Console.Out.WriteLineAsync(FiggleFonts.Slant.Render("Trelo Import")).ConfigureAwait(false);
            await Console.Out.WriteLineAsync($"{Assembly.GetExecutingAssembly().GetName().Name} {Assembly.GetExecutingAssembly().GetName().Version}").ConfigureAwait(false);

            await _importHelper.TestImportAsync();



            if (Debugger.IsAttached)
            {
                await Console.Out.WriteLineAsync("press any key to exit.").ConfigureAwait(false);
                Console.ReadKey();
            }
            _appLifetime.StopApplication();
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }


    class Program
    {
        private const string AppSettings = "appsettings.json";
        private const string HostSettings = "hostsettings.json";

        static Task Main(string[] args)
        {
            var consoleTracer = new ConsoleTraceListener(true);

            // Set the name of the trace listener, which helps identify this
            // particular instance within the trace listener collection.
            consoleTracer.Name = "mainConsoleTracer";

            // Write the initial trace message to the console trace listener.
            consoleTracer.WriteLine(DateTime.Now.ToString() + " [" + consoleTracer.Name + "] - Starting output to trace listener.");

            // Add the new console trace listener to
            // the collection of trace listeners.
            Trace.Listeners.Add(consoleTracer);

            var builder = new HostBuilder()
                .ConfigureHostConfiguration(configHost =>
                {
                    configHost.SetBasePath(Directory.GetCurrentDirectory());
                    configHost.AddJsonFile(HostSettings, optional: true);
                    configHost.AddCommandLine(args);
                })
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile(AppSettings, optional: true);
                    configApp.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
                    configApp.AddEnvironmentVariables(prefix: "Jira_");
                    configApp.AddCommandLine(args);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });



                    services.AddSingleton<ImportHelper>();
                    services.AddSingleton<ConfluenceSettings>();


                    services.AddSingleton<IHostedService, MainService>();
                    services.Configure<JiraImportSettings>(hostContext.Configuration.GetSection("jiraImport"));
                    services.Configure<ConfluenceSettings>(hostContext.Configuration.GetSection("confluence"));
                });

            return builder.RunConsoleAsync();
        }
    }

}
