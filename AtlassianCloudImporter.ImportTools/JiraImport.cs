﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtlassianCloudImporter.ImportTools
{
    class JiraImport
    {
        public TrelloImportBoard board { get; set; }
    }

    public class TrelloImportBoard
    {
        public TrelloImportItem[] items { get; set; }
    }

    public class TrelloImportItem
    {
        public string subject { get; set; }
        public string description { get; set; }
    }

}
