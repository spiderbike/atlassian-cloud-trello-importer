﻿using Atlassian.Jira;
using Atlassian.Jira.Remote;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AtlassianCloudImporter.ImportTools
{
    public class ImportHelper
    {
        public ConfluenceSettings _confluenceSettings { get; }

        private Jira _jira;

        public JiraImportSettings _jiraImportSettings { get; }



        public ImportHelper(IOptions<ConfluenceSettings> confluenceSettings, IOptions<JiraImportSettings> jiraImportSettings)
        {
            var settings = new JiraRestClientSettings();
            _confluenceSettings = confluenceSettings.Value;
            _jira = Jira.CreateRestClient(_confluenceSettings.Url, _confluenceSettings.Username, _confluenceSettings.Token, settings);
            _jiraImportSettings = jiraImportSettings.Value;
        }

        public async Task TestImportAsync()
        {
            using FileStream openStream = File.OpenRead(_jiraImportSettings.file);
            var trelloImport = await JsonSerializer.DeserializeAsync<JiraImport>(openStream);

            foreach (var issueToCreate in trelloImport.board.items)
            {
                var createIssues = new CreateIssueFields("TST");

                var issue = new Issue(_jira, createIssues);

                var remoteIssueType = new RemoteIssueType();
                remoteIssueType.name = "Bug";
                issue.Type = new IssueType(remoteIssueType);
                issue.Summary = issueToCreate.subject;
                issue.Description = issueToCreate.description;
                await _jira.Issues.CreateIssueAsync(issue);
            }
        }
    }
}
