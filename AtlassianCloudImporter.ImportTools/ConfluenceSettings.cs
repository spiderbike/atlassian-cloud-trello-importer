﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtlassianCloudImporter.ImportTools
{
    public class ConfluenceSettings
    {
        public string Url { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
